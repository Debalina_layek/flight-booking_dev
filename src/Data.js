export default {
  users: [
    {
      "username": "regular",
      "accessLevel": 1,
      "name": "Debalina Layek",
      "gender": "FEMALE",
      "email": "debalina.layek@cognizant.com",
      "password": "password",
      "picture": {"data": {
        "profilePic": "./img/user.jpg"
      }},
      "userID": "35800505220591"
    },
    {
      "username": "admin",
      "accessLevel": 0,
      "password": "password",
      "name": "Tony Stark",
      "gender": "Male",
      "email": "tony.stark@cognizant.com",
      "picture": {"data": {
        "profilePic": "./img/user.jpg"
      }},
      "userID": "35800505220592"
    }
  ],
  initProducts: [
    {
      name: "Air India",
      arivalTime: "10:00",
      departureTime: "8:00",
      duration: "2h 33m",
      flightNumber: "DL1051",
      tripType: "One Way",
      price: 399.99,
      toCity: "CCU",
      fromCity: "HYD",
      date: '26 Nov, 2020'
    },
    {
      name: "Indigo",
      arivalTime: "10:00",
      departureTime: "8:00",
      duration: "2h 33m",
      flightNumber: "DL1052",
      tripType: "One Way",
      price: 299.99,
      toCity: "CCU",
      fromCity: "HYD",
      date: '28 Nov, 2020'
    },
    {
      name: "Air Asia",
      arivalTime: "10:00",
      departureTime: "8:00",
      duration: "2h 33m",
      flightNumber: "MS1051",
      tripType: "One Way",
      price: 149.99,
      toCity: "ATL",
      fromCity: "MSP",
      date: '26 Dec, 2020'
    },
    {
      name: "Indigo",
      arivalTime: "10:00",
      departureTime: "8:00",
      duration: "2h 33m",
      flightNumber: "MS1052",
      tripType: "One Way",
      price: 109.99,
      toCity: "HYD",
      fromCity: "CCU",
      date: '16 Nov, 2020'
     }
  ]
};
