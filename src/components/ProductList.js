import React, { Fragment } from "react";
import ProductItem from "./ProductItem";
import withContext from "../withContext";

const ProductList = props => {
  const { products } = props.context;
  const { product } = props;
  return (
    <Fragment>
      <div className="container">
      <div className="promobannerrow mktBannerRow">
        <div className="promotionalBannerView promotionalBanners row flex-row-reverse splitBannerView hasBorder">
          <div className="promotionalBannerImageHolder d-sm-none d-md-block col-md-6 col-lg-7 col-xl-7 ng-star-inserted"><img border="0" className="promotionalBannerImage img-fluid" src="https://content.delta.com/content/dam/delta-applications/air-shopping/flightResults/dynamicBanner/noChangeFeesProd.png" alt="Peace-of-Mind Purchasing"/>
          </div>
          <div className="promotionalBannersTextContent col ng-star-inserted"><div className="row m-0"><div className="col-12 p-0"><div className="row m-0"><div className="col-12 p-0 headerCol">
            <h3 className="textAlignleft ng-star-inserted">Make your reservation</h3></div>
          <div className="col-12 p-0 descriptionCol ng-star-inserted"><div className="subheading textAlignleft">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi facere, soluta magnam consectetur molestias itaque
								ad sint fugit architecto incidunt iste culpa perspiciatis possimus voluptates aliquid consequuntur cumque quasi.
								Perspiciatis.</p>
</div></div></div></div></div></div></div></div>
        <div className="column columns is-multiline">
          {products && products.length ? (
            products.map((product, index) => (
              <ProductItem
                product={product}
                key={index}
                flightSelection={props.context.flightSelection}
              />
            ))
          ) : (
            <div className="column">
              <span className="title has-text-grey-light">
                No product found!
              </span>
            </div>
          )}
        </div>
      </div>
    </Fragment>
  );
};

export default withContext(ProductList);
