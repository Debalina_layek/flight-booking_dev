import React from "react";
import withContext from "../withContext";
import DatePicker from 'react-datepicker';
 
import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';

const SearchForm = props => {
  return (  
    <div className="container">
				<div className="row">
					<div className="col-md-7 col-md-push-5">
						<div className="booking-cta">
							<h1>Make your reservation</h1>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi facere, soluta magnam consectetur molestias itaque
								ad sint fugit architecto incidunt iste culpa perspiciatis possimus voluptates aliquid consequuntur cumque quasi.
								Perspiciatis.
							</p>
						</div>
					</div>
					<div className="col-md-4 col-md-pull-7">
						<div className="booking-form bg-fade">
							<form>
                            <div className="row tripselection"> 
                            
                                <div className={props.context.active === 'rt' ? 'selectiontab active' : 'selectiontab'} onClick={() => props.context.setFilter('rt')}>ROUND TRIP</div>
                           
                            
                            <div className={props.context.active === 'ow' ? 'selectiontab active' : 'selectiontab'} onClick={() => props.context.setFilter('ow')}>ONE WAY</div>
                         
</div>
                            <div className="row">
									<div className="col-sm-6">
                                    <div className="form-group">
									<span className="form-label">From</span>
									<input className="form-control" type="text" value={ props.context.fromcity } name="fromcity" onChange={ (event)=> props.context.handleChange(event) } placeholder="Enter a destination or hotel name"/>
									{props.context.fromerror && (
              <div className="has-text-danger">{props.context.fromerror}</div>
            )}
								</div>
                                        </div>
                                        <div className="col-sm-6">
                                        <div className="form-group">
									<span className="form-label">To</span>
									<input className="form-control" type="text" value={ props.context.tocity } name="tocity" onChange={ (event)=> props.context.handleChange(event) } placeholder="Enter a destination or hotel name"/>
									{props.context.toerror && (
              <div className="has-text-danger">{props.context.toerror}</div>
            )}
								</div>
                                        </div>
                                        </div>
								<div className="row">
									<div className="col-sm-4">
										<div className="form-group">
											<span className="form-label">Depart</span>
                                            <DatePicker
												selected={ props.context.depart }
												onChange={ (event)=> props.context.handleChange(event) }
												name="depart"
												dateFormat="MM/dd/yyyy"
											/>
										</div>
									</div>
                                    {props.context.active === 'rt' ? (
									<div className="col-sm-4">
										<div className="form-group">
											<span className="form-label">Return</span>
											<DatePicker
            selected={ props.context.return }
            onChange={ (event)=> props.context.handleChange(event) }
            name="return"
            dateFormat="MM/dd/yyyy"/>
										</div>
									</div>
                                     ) : (
                                        <div></div>
                                      )}
                                    <div className="col-sm-4">
										<div className="form-group">
											<span className="form-label">Class</span>
											<select className="form-control" name="fclass" value={ props.context.fclass } onChange={ (event)=> props.context.handleChange(event) }>
                                            <option value="">Class</option>
          <option value="economy">Economy</option>
          <option value="business">Business</option>
											</select>
											<span className="select-arrow"></span>
										</div>
									</div>
								</div>
								<div className="row">
									<div className="col-sm-6">
										<div className="form-group">
											<span className="form-label">Adults(12+ yrs)</span>
											<input type="number" className="form-control" value={ props.context.adult } name="adult" onChange={ (event)=> props.context.handleChange(event) }/> 
										</div>
									</div>
									<div className="col-sm-6">
										<div className="form-group">
											<span className="form-label">Child(2-11 yrs)</span>
                                            <input type="number" className="form-control" value={ props.context.child } name="child" onChange={ (event)=> props.context.handleChange(event) }/> 
										</div>
									</div>
								</div>
								<div className="form-btn">
									<button className="submit-btn" type="button" onClick={ props.context.onFormSubmit }>Check availability</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
); 
};

export default withContext(SearchForm);
