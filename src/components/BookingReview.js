import React, { Fragment } from "react";
import withContext from "../withContext";

const Booking = props => {
  const { data } = props.context.flightData;
  return (
    <Fragment>
    <div className="container">
      <div className="row">
        <div className="col-9">
          <div className="booking-form py-3">
            <h1 className="h4">Flight Details</h1>
            <div className=" column is-half">
            {props.context.flightData.product ? (  
            <div className="media">
                <div className="media-left">
                  <figure className="image is-64x64">
                    <img
                      src="https://bulma.io/images/placeholders/128x128.png"
                      alt="product"
                    />
                  </figure>
                  <b style={{ textTransform: "capitalize" }}>
                    {props.context.flightData.product.name}{" "}
                  </b>
                  <div>
                    <small>{props.context.flightData.product.date}</small>     
                  </div>             
                </div>
                <div className="media-content">
                  <div className="mediasection">
                    <div>{props.context.flightData.product.fromCity}</div>
                    <small>{props.context.flightData.product.departureTime}</small>
                  </div>

                  <div className="mediasection">
                    <div>{props.context.flightData.product.toCity}</div>
                    <small>{props.context.flightData.product.arivalTime}</small>
                  </div>
                  <div className="mediasection">
                    <button
                      className="button is-small is-outlined is-primary   is-pulled-right">
                      ${props.context.flightData.product.price}
                    </button>
                  </div>
                </div>
              </div>
          ) : (<div></div>)}
            </div>
          </div>
        </div>
        <div className="col-3">
          <div className="booking-form py-3 trip-total">
            <h1 className="h4">Trip Total</h1>
            <div className="text-left mb-2">2 Passengers</div>
            <div className="d-flex justify-content-between mb-2">
              <div className="justify-content-left">Flights</div>
              <div className="justify-content-right">$ 256.0</div>
            </div>
            <div className="d-flex justify-content-between">
              <div className="justify-content-left">Tax &amp; Charges</div>
              <div className="justify-content-right">$ 26.0</div>
            </div>
            <hr />
            <div className="d-flex justify-content-between">
              <div className="justify-content-left">Total</div>
              <div className="justify-content-right">$ 282.0</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="form-btn">
      <button className="submit-btn" type="button">Confirm</button>
    </div>
  
    </Fragment>
  );
};

export default withContext(Booking);
