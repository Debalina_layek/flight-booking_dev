import React from "react";

const ProductItem = props => {
  const { product } = props;
  let flightData = JSON.parse(localStorage.getItem("flightData"));
  product.toCity = flightData.tocity;
  product.fromCity = flightData.fromcity;
  return (
 <div className=" column is-half bg-fade whitebg">
 <div className="col-md-12 row cusmrgpad">
   <div className="col-md-5">
   <div className="row pt-1">
     <div className="col-6 pl0-xs-up pr0-xs-up"><div className="flightNumber">
  <div className="d-inline-block">{product.flightNumber}</div>
     </div></div><div className="col-6 pl0-xs-up"><div className="totalTime">
       <span className="sr-only">journey duration</span><span>{product.duration}</span></div></div>
   </div>
   <div className="row pt-2">
     <div className="col-5 pr0-md-up text-left dept-align">
     <span className="sr-only">departure time</span><span className="trip-time pr0-sm-down"> {product.departureTime}<sub>am</sub></span></div><div className="col-2 text-center"><span className="amenityIcons icon_flight"></span></div><div className="col-5 pl0-sm-down text-right"><span className="sr-only">arrival time</span>
   <span className="trip-time pl0-sm-down"> {product.arivalTime}<sub>am</sub><br/></span>
   </div>
   </div>
   <div className="row flightPathWrapper"><div className="col-12 flightPathStop">
     <div className="flightMark flL"></div><div className="flightMark abslRight"></div></div>
     <div className="col-12 flightStopWrapper"><div className="d-flex justify-content-between">
       <div className="flightSecFocus"><span className="sr-only">departure airport code</span> {product.fromCity} </div>
       <div className="flightSecFocus flightStopLayover">
         <span className="sr-only ng-star-inserted">arrival airport code</span>{product.toCity}</div>
         </div></div>
         <div className="col-12 flightStopWrapper pt-0"><span className="sr-only">number of stops</span>
   <div className="nonstop">Nonstop</div></div></div>
   </div>
   <div className="col-md-7 pr-0">
<div className="classtypeparent businessclasstype" onClick={() =>
      props.flightSelection({
        id: product.name,
        product,
        type: 'business',
        amount: 1
      })
    }>
<div className="col-12 hidden-md-down bottomladdercell"></div>
<p>Business</p>
<p className="flightcost">$<span>{product.price}</span></p>
<p className="triptypetext">{product.tripType}</p>
</div>
<div className="classtypeparent economyclasstype" onClick={() =>
      props.flightSelection({
        id: product.name,
        product,
        type: 'economy',
        amount: 1
      })
    }>
<div className="col-12 hidden-md-down bottomladdercell1"></div>
<p>Economy</p>
<p className="flightcost">$<span>{product.price}</span></p>
<p className="triptypetext">{product.tripType}</p>
</div>
<div className="classtypeparent mainclasstype" onClick={() =>
      props.flightSelection({
        id: product.name,
        product,
        type: 'main',
        amount: 1
      })
    }>
<div className="col-12 hidden-md-down bottomladdercell2"></div>
<p>Main</p>
<p className="flightcost">$<span>{product.price}</span></p>
<p className="triptypetext">{product.tripType}</p>
</div>
   </div>
   </div>
    </div>
  );
};

export default ProductItem;
